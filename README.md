![](https://gitlab.com/PatrickLerner/MiniScheme/raw/master/etc/logo.png)

[![pipeline status](https://gitlab.com/PatrickLerner/MiniScheme/badges/master/pipeline.svg)](https://gitlab.com/PatrickLerner/MiniScheme/commits/master)
[![coverage report](https://gitlab.com/PatrickLerner/MiniScheme/badges/master/coverage.svg)](https://gitlab.com/PatrickLerner/MiniScheme/commits/master)

Bestest of schemes in the world for Lunch and Learn demonstration at Instaffo!

## REPL

1. `bundle`
2. `rake`
