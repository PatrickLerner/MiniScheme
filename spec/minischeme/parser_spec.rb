# frozen_string_literal: true

require 'spec_helper'
require_relative '../../minischeme/parser'

describe Parser do
  let(:result) { described_class.parse(program) }
  let(:first_value) { result.values.first }

  describe 'empty program' do
    let(:program) { '' }

    it 'empty string raises error' do
      expect { result }.to raise_error(RuntimeError)
    end
  end

  describe 'print function' do
    let(:program) { '(print "Hello World")' }

    it 'splits tokens correctly' do
      expect(result).to be_an_instance_of(Operator::Print)
      expect(first_value).to be_an_instance_of(Constant::String)
      expect(first_value.value).to eq('Hello World')
    end
  end

  describe 'calculation function' do
    let(:program) { '(print (+ 2 3))' }

    it 'splits tokens correctly' do
      expect(result).to be_an_instance_of(Operator::Print)
      expect(first_value).to be_an_instance_of(Operator::Addition)
      expect(first_value.values.map(&:class)).to eq([Constant::Number, Constant::Number])
      expect(first_value.values.map(&:value)).to eq([2, 3])
    end
  end

  describe 'unknown operator' do
    let(:program) { '(nikolai)' }

    it 'raises exception' do
      expect { result }.to raise_error(ArgumentError)
    end
  end

  describe 'unknown value' do
    let(:program) { '(print nikolai)' }

    it 'raises exception' do
      expect { result }.to raise_error(ArgumentError)
    end
  end
end
