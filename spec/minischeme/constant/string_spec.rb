# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../minischeme/constant/string'

describe Constant::String do
  describe 'execute' do
    let(:value) { "\"#{normalized_value}\"" }
    let(:normalized_value) { 'Ahmet Murati' }

    subject { described_class.new(value) }

    it 'executes as value' do
      expect(subject.execute).to eq(normalized_value)
    end
  end
end
