# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../minischeme/constant/number'

describe Constant::Number do
  describe 'execute' do
    let(:value) { 2 }

    subject { described_class.new(value) }

    it 'executes as value' do
      expect(subject.execute).to eq(value)
    end
  end
end
