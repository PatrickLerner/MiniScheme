# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../minischeme/operator/addition'

describe Operator::Addition do
  describe 'execute' do
    subject { described_class.new(values) }

    context 'empty values' do
      let(:values) { [] }

      it 'returns zero' do
        expect(subject.execute).to be_zero
      end
    end

    context 'sum of execute returns' do
      def number_double(value)
        dbl = double
        allow(dbl).to receive(:execute).and_return(value)
        dbl
      end

      let(:one) { number_double(1) }
      let(:two) { number_double(2) }

      let(:values) { [one, two] }

      it 'returns three' do
        expect(subject.execute).to eq(3)
      end
    end
  end
end
