# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../minischeme/operator/print'

describe Operator::Print do
  describe 'execute' do
    subject { described_class.new(values) }

    context 'empty values' do
      let(:values) { [] }

      it 'prints empty line' do
        expect(subject).to receive(:puts).with('')
        subject.execute
      end
    end

    context 'multiple values' do
      def string_double(value)
        dbl = double
        allow(dbl).to receive(:execute).and_return(value)
        dbl
      end

      let(:one) { string_double(1) }
      let(:two) { string_double('Hello World!') }

      let(:values) { [one, two] }

      it 'prints empty line' do
        expect(subject).to receive(:puts).with('1Hello World!')
        subject.execute
      end
    end
  end
end
