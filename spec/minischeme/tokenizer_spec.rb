# frozen_string_literal: true

require 'spec_helper'
require_relative '../../minischeme/tokenizer'

describe Tokenizer do
  let(:result) { described_class.tokenize(program) }

  describe 'empty string' do
    let(:program) { '' }

    it 'empty string becomes empty array' do
      expect(result).to be_empty
    end
  end

  describe 'basic calculation' do
    let(:program) { '(+ 2 3)' }

    it 'splits tokens correctly' do
      expect(result).to eq(['(', '+', '2', '3', ')'])
    end
  end

  describe 'basic calculation' do
    let(:program) { '(print "Hello World!" 1 2 3)' }

    it 'splits tokens correctly' do
      expect(result).to eq(['(', 'print', '"Hello World!"', '1', '2', '3', ')'])
    end
  end
end
