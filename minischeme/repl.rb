# frozen_string_literal: true

# !/usr/bin/env ruby

require_relative './parser'

begin
  loop do
    print '> '
    code = gets
    exit 0 if code.nil?
    result = Parser.parse(code).execute
    puts "=> #{result}" if result
  rescue StandardError => e
    puts "!! #{e.message}"
  end
rescue Interrupt
  puts 'ok.'
end
