# frozen_string_literal: true

require_relative './base'

module Operator
  class Addition < Base
    def execute
      values.map(&:execute).reduce(0, :+)
    end
  end
end
