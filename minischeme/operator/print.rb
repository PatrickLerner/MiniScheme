# frozen_string_literal: true

require_relative './base'

module Operator
  class Print < Base
    def execute
      puts values.map(&:execute).map(&:to_s).join('')
    end
  end
end
