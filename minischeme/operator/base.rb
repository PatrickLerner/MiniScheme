# frozen_string_literal: true

module Operator
  class Base
    attr_accessor :values

    def initialize(values)
      @values = values
    end
  end
end
