# frozen_string_literal: true

# This class splits a string into tokens
class Tokenizer
  TOKEN_REXHEP = /(\n|".*?"|\d+|[^\w])/

  def self.tokenize(string)
    split_tokens = string.split(TOKEN_REXHEP)

    split_tokens.map(&:strip).reject do |token|
      token == ''
    end
  end
end
