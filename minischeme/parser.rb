# frozen_string_literal: true

require_relative './tokenizer'
require_relative './operator/print'
require_relative './operator/addition'
require_relative './constant/string'
require_relative './constant/number'

class Parser
  def self.parse(code)
    new(code).send(:ast)
  end

  OPERATORS = {
    print: Operator::Print,
    '+': Operator::Addition,
  }

  INSTRUCTION_BEGIN = '('
  INSTRUCTION_END = ')'

  protected

  attr_accessor :tokens

  def initialize(code)
    @tokens = Tokenizer.tokenize(code).reverse
  end

  def ast
    @ast ||= parse_instruction
  end

  def accept?(token, skip: true)
    if next_token == token
      next_token! if skip
      return true
    end
    false
  end

  def expect!(token)
    return if accept?(token)
    raise "Expected token #{token}, but found #{next_token}"
  end

  def accept_number?
    next_token.match?(/^[0-9]+$/)
  end

  def accept_string?
    next_token.match?(/^".*"$/)
  end

  def next_token
    tokens.last
  end

  def next_token!
    tokens.pop
  end

  def parse_instruction
    expect!(INSTRUCTION_BEGIN)
    operator_class = parse_operator
    values = []
    values << parse_value until accept?(INSTRUCTION_END)
    operator_class.new(values)
  end

  def parse_operator
    OPERATORS.each do |key, klass|
      return klass if accept?(key.to_s)
    end

    raise ArgumentError, "unknown operator #{next_token}"
  end

  def parse_value
    if accept_number?
      parse_number
    elsif accept_string?
      parse_string
    elsif accept?(INSTRUCTION_BEGIN, skip: false)
      parse_instruction
    else
      raise ArgumentError, "unknown type of parameter '#{next_token}'"
    end
  end

  def parse_number
    Constant::Number.new(next_token!)
  end

  def parse_string
    Constant::String.new(next_token!)
  end
end
