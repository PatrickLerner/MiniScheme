# frozen_string_literal: true

require_relative './base'

module Constant
  class String < Base
    def initialize(value)
      @value = value[1..-2]
    end
  end
end
