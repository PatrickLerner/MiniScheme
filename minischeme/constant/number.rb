# frozen_string_literal: true

require_relative './base'

module Constant
  class Number < Base
    def initialize(value)
      @value = value.to_i
    end
  end
end
