# frozen_string_literal: true

module Constant
  class Base
    attr_accessor :value

    def execute
      value
    end
  end
end
